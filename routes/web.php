<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// TEST ROUTE
// Route::get('/test', function(){
// 	return view('test');
// })->middleware('role:ROLE_DOCTOR');
Route::get('/test', function(){
	return view('test');});
// ----------------------
// Route::get('/loginOld', function(){
// 	return view('/auth.loginOld');
// });
Route::post('/test','TestController@store');

Auth::routes();

// Route::post('/login', 'Auth\LoginController@login');
Route::middleware('auth')->group(function(){

	Route::get('/logout', 'Auth\LoginController@logout');
	Route::get('/home', 'HomeController@index');
	Route::get('/', 'HomeController@index');
	Route::post('/patientSearch', 'HomeController@search');

	// DOCTOR Routes     
	Route::get('/doctors','DoctorController@index');
	Route::get('/doctors/getTable','DoctorController@getTable');
	Route::get('/adddoctor','DoctorController@create');
	Route::post('/adddoctor','DoctorController@store');
	Route::get('/doctor/{id}','DoctorController@show');
	Route::get('/doctor/{id}/update','DoctorController@edit');
	Route::put('/doctor/{id}/update','DoctorController@update');
	Route::get('/doctor/{id}/delete','DoctorController@destroy');
	Route::post('/docqualification/','DoctorController@DocQualUpdate');

	// Qualification Routes
	Route::get('/qualifications','QualificationController@index');
	Route::get('/qualifications/getTable','QualificationController@getTable');
	Route::get('/addqualification','QualificationController@create');
	Route::post('/addqualification','QualificationController@store');
	Route::get('/qualification/{id}','QualificationController@show');
	Route::get('/qualification/{id}/update','QualificationController@edit');
	Route::put('/qualification/{id}/update','QualificationController@update');
	Route::get('/qualification/{id}/delete','QualificationController@destroy');

	// ADMIN Routes
	Route::get('/admin','AdminController@index')->middleware('role:ROLE_ADMIN');
	Route::get('/adduser','AdminController@create')->middleware('role:ROLE_ADMIN');
	Route::post('/adduser','AdminController@UserStore');

	// Patient Routes
	Route::get('/patients','PatientController@index');
	Route::get('/patients/getTable','PatientController@getTable');
	Route::get('/addpatient','PatientController@create');
	Route::post('/addpatient','PatientController@store');
	Route::get('/patient/{id}','PatientController@show');
	Route::get('/patient/{id}/update','PatientController@edit');
	Route::put('/patient/{id}/update','PatientController@update');
	Route::get('/patient/{id}/delete','PatientController@destroy');

	// Checkup Routes
	Route::get('/checkups' ,'CheckupController@index');
	Route::get('/checkups/getTable' ,'CheckupController@getTable');
	Route::get('/addcheckup' ,'CheckupController@create');
	Route::post('/addcheckup' ,'CheckupController@store');
	Route::get('/checkup/{id}','CheckupController@show');
	Route::get('/checkup/{id}/update' ,'CheckupController@edit');
	Route::post('/checkup/{id}/update' ,'CheckupController@update');
	Route::get('/checkup/{id}/delete' ,'CheckupController@destroy');
	Route::post('/checkup/search' ,'CheckupController@search');

	// Reports Route
	Route::get('/report/patient/{id}','ReportController@PatientReport');
	Route::get('/report/doctor/{id}','ReportController@DoctorReport');

	// Prescription
	Route::get('/checkup/{id}/prescription' ,'PrescriptionController@index');
	Route::get('/checkup/{id}/addprescription' ,'PrescriptionController@create');
	Route::post('/addprescription' ,'PrescriptionController@store');
	Route::get('prescription/{id}/delete' ,'PrescriptionController@destroy');

});

