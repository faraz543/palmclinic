<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
        	[
            'name' => 'ROLE_ADMIN',
            'description' => 'Admin Access',
    	    ],
    	    [
            'name' => 'ROLE_DOCTOR',
            'description' => 'Doctor Access',
    	    ],
    	]);
    	DB::table('role_user')->insert([
    		'user_id'=>'1',
    		'role_id'=>'1'
    	]);
    }
}
