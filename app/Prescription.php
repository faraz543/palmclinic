<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Prescription extends Model
{
    use SoftDeletes;

    protected $fillable = ['session_id', 'details', 'prescription','images','checkup_id','date'];

    public function patient($value) {
        return $this->belongsTo('App\Checkup');
    }
}
