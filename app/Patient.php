<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Scopes;

class Patient extends Model
{
    // protected static function boot(){
    //     parent::boot();

    //     static::addGlobalScope(new AllScope);
    // }

    use SoftDeletes;

    protected $fillable = ['name', 'age', 'gender', 'address','info','user_id','phone_number'];

    public function user(){
        return $this->hasOne('App\User');
    }

    public function setInfoAttribute($value) {
        $this->attributes['info'] = json_encode($value);
    }

    public function getTemplateAttribute($value) {
        return json_decode($value);
    }

    public function sessions(){
    	return $this->hasMany('App\Session');
    }

    public function prescritions(){
    	return $this->hasMany('App\Prescription');
    }

    public function checkups(){
        return $this->hasMany('App\Checkup');
    }
}
