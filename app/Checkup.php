<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Checkup extends Model
{
    protected $fillable = ['patient_id','doctor_id','reason','date','start_time','end_time'];

    public function doctor(){
    	return $this->belongsTo('App\Doctor');
    }
    public function patient(){
    	return $this->belongsTo('App\Patient');
    }
    public function prescription(){
    	return $this->hasOne('App\Prescription');
    }
}
