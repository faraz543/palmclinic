<?php

namespace App\Helpers;

use Form;
use Html;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class Helper{

	public static function form_view(array $fields,array $info){
		echo Form::open([
			'method'=>$info['method'],
			'action'=>$info['action'] ,
			'class'=>'jumbotron',
			'files'=>'true',
		]);
		echo "<h3 id='form-title' class='form-group'>".$info['form_title']."</h3>";
			foreach ($fields as $field) {
				echo Form::custom_field($field);
			}?>
			<div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
               			Submit
                    </button>
                </div>
            </div>
		<?php echo Form::close();
	}

	public static function FormValidation($request,$rules){
        $validated = Validator::make($request->all(),$rules);
        return $validated;
    }

}

?>