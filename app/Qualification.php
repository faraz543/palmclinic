<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Qualification extends Model
{
    protected $fillable = ['name','description'];

    public function doctors(){
    	return $this->belongstoMany('App\Doctor')->withTimestamps();
    }
}
