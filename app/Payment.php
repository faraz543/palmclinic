<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;

    protected $fillable = ['session_id', 'date', 'fee', 'free'];

    public function patient($value) {
        return $this->belongsTo('App\Session');
    }
}
