<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    

	protected $fillable = ['name','gender','address','qualification','info','user_id','phone_number'];

	public function user(){
        return $this->hasOne('App\User');
    }

    public function qualifications(){
    	return $this->belongstoMany('App\Qualification');
    }

    public function checkups(){
    	return $this->hasMany('App\Checkup');
    }
}
