<?php 
namespace App\Providers;

use Form;
use Illuminate\Support\ServiceProvider;

class FormMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(){
        Form::macro('custom_field',function($field){
            if($field['type'] != 'hidden'){
                echo "<div class='form-group row'>";
                echo Form::label(
                    $field['name'],
                    ( !empty($field['label']) ? $field['label'] : $field['name']  ),
                    ['class' => 'col-md-4 col-form-label text-md-right']
                );
                echo "<div class='col-md-6'>";
            }
            switch($field['type']){
                case "email":
                    echo Form::email(
                        $field['name'],
                        (!empty($field['value']) ? $field['value'] : ''),
                        $field['attributes']
                    );
                    break;
                case "text":
                    echo Form::text(
                        $field['name'],
                        (!empty($field['value']) ? $field['value'] : ''),
                        $field['attributes']
                    );
                    break;
                    case "number":
                    echo Form::number(
                        $field['name'],
                        (!empty($field['value']) ? $field['value'] : ''),
                        $field['attributes']
                    );
                    break;
                case "password":
                    echo Form::password($field['name'],$field['attributes']);
                    break;
                case 'select':
                    echo Form::select($field['name'],$field['options'],(!empty($field['value']) ? $field['value'] : ''),$field['attributes']);
                    break;
                case 'textarea':
                    echo Form::textarea(
                        $field['name'],
                        (!empty($field['value']) ? $field['value'] : ''),
                        $field['attributes']
                    );
                    break;
                case 'hidden':
                    echo Form::hidden($field['name'],$field['value']);
                    break;
                case 'file':
                    echo Form::file($field['name'],$field['attributes']);
                    break;
            }
            if($field['type'] != 'hidden'){
            echo "</div></div>";}
        }); 
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
?>