<?php

namespace App\Http\Middleware;

use Closure;
use User;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if(!$request->user()->hasRole($role)){
            abort(401,'This Action in Unautorized');
        }
        return $next($request);
    }
}
