<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;
use Helper;
use DataTables;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('patient.index');
    }

    public function getTable(){
        $query = Patient::get();
        return Datatables::of($query)
                ->addColumn('update',function(Patient $patient){
                    return "<a href=".url("/patient/$patient->id/update")."><i class='mdi mdi-tooltip-edit'></i></a>";
                })
                ->addColumn('delete',function(Patient $patient){
                    return "<a data-toggle='modal' data-target='#deleteModal' href=$patient->id class='delModalTrigger'><i class='mdi mdi-delete'></i></a>";
                })
                ->rawColumns(['update'=>'update','delete'=>'delete'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fields = [
            [
                'label'=>'Name',
                'name'=>'name',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'Phone Number',
                'name'=>'phone_number',
                'type'=>'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'Age',
                'name'=>'age',
                'type' => 'number',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'age'         
                    ]
            ],
            [
                'label'=>'Address',
                'name'=>'address',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'address'         
                    ]
            ],
            [
                'label'=>'Select Gender',
                'name'=>'gender',
                'type' => 'select',
                'options'=>[
                    'male' => 'Male',
                    'female' => 'Female'
                ],
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'gender'         
                    ]
            ],
            // [
            //     'label'=>'Additional Info',
            //     'name'=>'info',
            //     'type'=>'textarea',
            //     'attributes' =>[
            //             'class' => 'form-control',
            //             'id' => 'info'         
            //         ]
            // ]
        ];
        $form_info = ['method'=>'POST','action'=>'PatientController@store','form_title'=>'Add Patient'];
        return view('patient.add',compact('fields','form_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'phone_number'=>'required|digits:11',
            'age' => 'integer|required',
            'address'=>'required',
            'gender'=>'required',
            // 'info'=>'required'
        ];
        $validator = Helper::FormValidation($request,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
            if($newpatient = Patient::create($request->all())){
                return redirect('/patients')->with('success','Addition Successful');
            }else{
                return redirect()->back()->withErrors(['Addition Failed']);
            } 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = Patient::find($id);
        return (!empty($record) ? $record : 'not found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($patient = Patient::find($id)){
            $fields = [
                [
                    'label'=>'Name',
                    'name'=>'name',
                    'type' => 'text',
                    'value'=> $patient->name,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'name'          
                        ]
                ],
                [
                    'label'=>'Age',
                    'name'=>'age',
                    'type' => 'number',
                    'value'=>$patient->age,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'age'         
                        ]
                ],
                [
                    'label'=>'Address',
                    'name'=>'address',
                    'type' => 'text',
                    'value'=>$patient->address,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'address'         
                        ]
                ],
                [
                    'label'=>'Select Gender',
                    'name'=>'gender',
                    'type' => 'select',
                    'value'=>$patient->gender,
                    'options'=>[
                        'male' => 'Male',
                        'female' => 'Female'
                    ],
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'gender'         
                        ]
                ],
            ];
            $form_info = 
            ['method'=>'PUT','action'=>['PatientController@update',$patient->id],'form_title'=>'Update Patient'];
            return view('patient.edit',compact('fields','form_info'));
        }else{
            return redirect()->back()->withErrors(['Record Not Found']);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return 'here';
        $rules = [
            'name' => 'required',
            'age' => 'integer|required',
            'address'=>'required',
            'gender'=>'required',
        ];
        $validator = Helper::FormValidation($request,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
            $patient = Patient::where('id',$id)->first();
            if($updatedPatient = $patient->update($request->all())){
                return redirect()->back()->with('success','Updation Successful');
            }else{
                return redirect()->back()->withErrors(['Updation Failed']);
            }
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Patient::destroy($id)){
            return redirect()->back()->withErrors(['Deletion Successful']);
        }else{
            return redirect()->back()->withErrors(['Deletion Failed']);;
        }
    }
}
