<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkup;
use App\Doctor;
use App\Patient;

class ReportController extends Controller
{
    // public function PatientReport($id){
    // 	if(Patient::find($id)){
    // 		$report = Checkup::where('patient_id',$id)->with('patient')->get();
    // 		return (count($report) == 0 ? 'No Checkups' : $report);
    // 	}else{
    // 		return 'Patient do not Exist';
    // 	}
    // }

    // public function DoctorReport($id){
    // 	if(Doctor::find($id)){
    // 		$report = Checkup::where('doctor_id',$id)->with('patient')->get();
    // 		return (count($report) == 0 ? 'No Checkups' : $report);
    // 	}else{
    // 		return 'Doctor do not Exist';
    // 	}
    // }
    
    public function DoctorReport($id, Request $request){
    	if(Doctor::find($id)){
    		// return $request->all();
    		$report = Checkup::whereBetween('date', [$request['from'], $request['to']])->get();
    		return (count($report) == 0 ? 'No Checkups' : $report);
    	}else{
    		return 'Doctor do not Exist';
    	}
    }
}
