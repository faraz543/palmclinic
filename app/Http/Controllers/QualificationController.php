<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Qualification;
use Helper;
use DB;
use DataTables;

class QualificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('qualification.index');
    }

    public function getTable(){
        $query = Qualification::get();
        return Datatables::of($query)
                ->addColumn('update',function(Qualification $qualification){
                    return "<a href=".url("/qualification/$qualification->id/update")."><i class='mdi mdi-tooltip-edit'></i></a>";
                })
                ->addColumn('delete',function(Qualification $qualification){
                    return "<a data-toggle='modal' data-target='#deleteModal' href=$qualification->id class='delModalTrigger'><i class='mdi mdi-delete'></i></a>";
                })
                ->rawColumns(['update'=>'update','delete'=>'delete'])
                ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fields = [
            [
                'label'=>'Name',
                'name'=>'name',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'description',
                'name'=>'description',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'description'         
                    ]
            ]
        ];
        $form_info = ['method'=>'POST','action'=>'QualificationController@store','form_title'=>'Add Qualification'];
        return view('qualification.add',compact('fields','form_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name' => 'required|unique:qualifications',
            'description'=>'nullable'
        ];
        $validator = Helper::FormValidation($request,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
            if($newQualification = Qualification::create($request->all())){
                return redirect('/qualifications')->with('success','Qualification Addition Successful');
            }else{
                return redirect()->back()->withErrors(['Addition Failed']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return (Qualification::find($id) ? Qualification::find($id) : 'no record found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($qualification = Qualification::find($id)){
            $fields = [
                [
                    'label'=>'Name',
                    'name'=>'name',
                    'type' => 'text',
                    'value'=>$qualification->name,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'name'          
                        ]
                ],
                [
                    'label'=>'description',
                    'name'=>'description',
                    'value'=>$qualification->description,
                    'type' => 'text',
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'description'         
                        ]
                ]
            ];
            $form_info = 
            ['method'=>'PUT','action'=>['QualificationController@update',$qualification->id],'form_title'=>'Update Qualification'];
            return view('qualification.edit',compact('fields','form_info'));
        }else{

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $rules = [
                'name' => 'required',
                'description'=>'nullable'
            ];
            $validator = Helper::FormValidation($request,$rules);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }else{
                $qualification = Qualification::where('id',$id)->first();
                if($updatedQualification = $qualification->update($request->all())){
                    return redirect()->back()->with('success','Addition Successful');
                }else{
                    return redirect()->back()->withErrors(['Updation Failed']);
                }
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Qualification::destroy($id)){
            return redirect()->back()->withErrors(['Deletion Successful']);
        }else{
            return redirect()->back()->withErrors(['Deletion Failed']);;
        }
    }    
}
