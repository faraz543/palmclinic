<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Patient;

use App\Doctor;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all()->pluck('name','id');
        return view('home',compact('doctors'));
    }
    
    public function search(Request $request){
        $search = $request['search'];
        if(!empty($search)){
            if(is_numeric($search)){
                $patient = Patient::where('phone_number','like',"%$search%")->orWhere('id',$search)->pluck('id','name');
            }else{
                $patient = Patient::where('name','like',"%$search%")->pluck('id','name');
            }
            return response()->json(['items'=>$patient]);
        }
    }

}
