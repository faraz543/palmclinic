<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Checkup;
use App\Doctor;
use Helper;
use DataTables;

class CheckupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $checkups = Checkup::with('doctor','patient','prescription')->get();
        return view('checkup.index');
    }

    public function getTable(){
        $query = Checkup::with('doctor','patient')->select('checkups.*');
        return Datatables::eloquent($query)
                ->addColumn('patientNameNum',function(Checkup $checkup){
                    return $checkup->patient->name.'<br>Phone: '.$checkup->patient->phone_number;
                })
                ->addColumn('prescription',function(Checkup $checkup){
                    return 
                    ($checkup->prescription ? 
                    "<a class='btn btn-primary' href=checkup/".$checkup->id.'/prescription>View Prescription</a>' : "<a data-toggle='modal' data-target='#AddPrescription' class='btn btn-success addModalTrigger' href =".$checkup->id.">Add Prescription +</a>"
                    );
                })
                ->addColumn('update',function(Checkup $checkup){
                    return "<a href=".url("/checkup/$checkup->id/update")."><i class='mdi mdi-tooltip-edit'></i></a>";
                })
                ->addColumn('delete',function(Checkup $checkup){
                    return "<a data-toggle='modal' data-target='#deleteModal' href=$checkup->id class='delModalTrigger'><i class='mdi mdi-delete'></i></a>";
                })
                ->rawColumns(['update'=>'update','delete'=>'delete','prescription'=>'prescription','patientNameNum'=>'patientNameNum'])
                ->make(true);
    }
    // public function 
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $doctors = Doctor::all()->pluck('name','id');
        $fields = [
            [
                'label'=>'Date',
                'name'=>'date',
                'type'=>'text',
                'attributes'=>[
                    'class'=> 'form-control datepicker',
                    'id'=>'date',
                    'data-provide'=>'datepicker'
                ]
            ], 
            [
                'label'=>'Reason',
                'name'=>'reason',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'Start Time',
                'name'=>'start_time',
                'type'=>'text',
                'attributes' =>[
                        'class' => 'form-control clockpicker',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'End Time',
                'name'=>'end_time',
                'type'=>'text',
                'attributes' =>[
                        'class' => 'form-control clockpicker',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'Doctor',
                'name'=>'doctor_id',
                'type'=>'select',
                'options'=> $doctors,
                'attributes' =>[
                    'class' => 'form-control ',
                    'id' => 'doctor_id',
                ]
            ],
        ];
        return view('checkup.add',compact('fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $rules = [
            'patient_id' => 'integer|required',
            'doctor_id' => 'integer|required',
            'reason' => 'required',
            'date'=>'required|date'
        ];
        $validator = Helper::FormValidation($request,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }else{
            if($newCheckup = Checkup::create($request->all())){
                return redirect("/checkups")->with('success','Checkup Added');
            }else{
                return redirect()->back()->withErrors(['Addition Failed']);            
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return (Checkup::find($id) ? 
                    Checkup::with('doctor','patient','prescription')->where('id',$id)->get() :
                    'no record found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 'Update Template';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return 'here';
        if(Checkup::find($id)){
            $rules = [
                // 'patient_id' => 'integer|required',
                // 'doctor_id' => 'integer|required',
                'reason' => 'required',
                'date'=>'required|date|date_format:d-m-Y'
            ];
            $validator = Helper::FormValidation($request,$rules);
            if($validator->fails()){
                return $validator->messages();
            }else{
                if($updatedCheckup = Checkup::where('id',$id)->update($request->all())){
                    return Checkup::find($id); 
                }else{
                    return 'updation failed';
                }
            }
        }else{
            return 'record not found';
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Checkup::destroy($id)){
            return Checkup::all();
        }else{
            return 'not found' ;
        }
    }

    public function search(Request $request){
        // return response()->json([$request->all()]);
        $checkups = Checkup::with('patient','prescription')->where('patient_id',$request['search'])->where('doctor_id',$request['doctor_id'])->orderBy('date', 'desc')->limit(5)->get();;
        return response()->json($checkups);
    }
}
