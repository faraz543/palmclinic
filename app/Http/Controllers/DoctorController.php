<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Doctor;
use App\Qualification;
use Helper;
use Illuminate\Support\Collection;
use DataTables;

class DoctorController extends Controller
{
    public function __construct(){
        // $this->middleware('auth');
        // $this->middleware('role:ROLE_DOCTOR');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('doctor.index');
    }

    public function getTable(){
        $query = Doctor::with('qualifications')->select('doctors.*');
        return Datatables::eloquent($query)
                ->addColumn('qualifications', function (Doctor $doctor) {
                    return $doctor->qualifications->pluck('name')->implode(', ');
                })
                ->addColumn('update',function(Doctor $doctor){
                    return "<a href=".url("/doctor/$doctor->id/update")."><i class='mdi mdi-tooltip-edit'></i></a>";
                })
                ->addColumn('delete',function(Doctor $doctor){
                    return "<a data-toggle='modal' data-target='#deleteModal' href=$doctor->id class='delModalTrigger'><i class='mdi mdi-delete'></i></a>";
                })
                ->rawColumns(['update'=>'update','delete'=>'delete'])
                ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $qualifications = Qualification::all()->pluck('name','id');
        $fields = [
            [
                'label'=>'Name',
                'name'=>'name',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'name'          
                    ]
            ],
            [
                'label'=>'Phone Number',
                'name'=>'phone_number',
                'type'=>'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'phone_number'          
                    ]
            ],
            [
                'label'=>'Address',
                'name'=>'address',
                'type' => 'text',
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'address'         
                    ]
            ],
            [
                'label'=>'Select Gender',
                'name'=>'gender',
                'type' => 'select',
                'options'=>[
                    'male' => 'Male',
                    'female' => 'Female'
                ],
                'attributes' =>[
                        'class' => 'form-control',
                        'id' => 'gender'         
                    ]
            ],
            [
                'label'=>'Qualification',
                'name'=>'qualifications[]',
                'type'=>'select',
                'options'=> $qualifications,
                'attributes' =>[
                    'class' => 'form-control qualification-field',
                    'id' => 'gender',
                    'multiple'=>'multiple'         
                ]
            ],
        ];
        $form_info = ['method'=>'POST','action'=>'DoctorController@store','form_title'=>'Add Doctor'];
        return view('doctor.add', compact('fields','form_info'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request;
        $rules = [
            'name' => 'required',
            'phone_number'=>'required|digits:11',
            'gender' => 'required',
            'address'=>'required',
            'qualifications'=>'required',
        ];
        $validator = Helper::FormValidation($request, $rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator);
        }else{
        // return $request;
            if($newDoctor = Doctor::create($request->all())){
                if($newDoctor->qualifications()->sync($request['qualifications'])){
                    return redirect('doctors')->with('success','Record Added Successful'); 
                }
                else{
                    return redirect()->back()->withErrors('Try Again');
                }
            }
            else{
                return redirect()->back()->withErrors('Addition Failed');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return (Doctor::find($id) ? Doctor::find($id) : 'no record found');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Doctor::find($id)){
            $qualifications = Qualification::all()->pluck('name','id');
            // $qualifications_id = $qualifications->pluck('name','id');
            $doctor = Doctor::where('id',$id)->with('qualifications')->get()->first();
            // return $doctor->id;
            $fields = [
                [
                    'label'=>'Name',
                    'name'=>'name',
                    'type' => 'text',
                    'value'=>$doctor->name,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'name'          
                        ]
                ],
                [
                    'label'=>'Address',
                    'name'=>'address',
                    'type' => 'text',
                    'value'=>$doctor->address,
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'address'         
                        ]
                ],
                [
                    'label'=>'Select Gender',
                    'name'=>'gender',
                    'type' => 'select',
                    'value'=>$doctor->gender,
                    'options'=>[
                        'male' => 'Male',
                        'female' => 'Female'
                    ],
                    'attributes' =>[
                            'class' => 'form-control',
                            'id' => 'gender'         
                        ]
                ],
            ];
            $qualification_fields =  [
                [
                    'label'=>'Qualification',
                    'name'=>'qualifications[]',
                    'type'=>'select',
                    'value'=>$doctor->qualifications,
                    'options'=> $qualifications,
                    'attributes' =>[
                        'class' => 'form-control qualification-field',
                        'id' => 'gender',
                        'multiple'=>'multiple'         
                    ]
                ],
                [
                    'type' => 'hidden',
                    'name' => 'user_id',
                    'value' => $doctor->id
                ]
            ];
            // return $doctor->qualifications;
            $qualification_form = 
            ['method'=>'POST','action'=>'DoctorController@DocQualUpdate','form_title'=>'Update Doctor Qualifications'];
            $form_info = 
            ['method'=>'PUT','action'=>['DoctorController@update',$doctor->id],'form_title'=>'Update Doctor'];
            return view('doctor.edit', compact('fields','form_info','qualification_fields','qualification_form'));
        }else{
            return redirect()->back()->withErrors(['Record Not Found']); 
        }
        // return view('doctor.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $rules = [
                'name' => 'required',
                'gender' => 'required',
                'address'=>'required',
            ];
            $validator = Helper::FormValidation($request, $rules);
            if($validator->fails()){
                return redirect()->back()->withErrors($validator);
            }else{
                $doctor = Doctor::where('id',$id)->first();
                if($updatedDoctor = $doctor->update($request->all())){
                    return redirect()->back()->with('success','Info Updation Successful'); 
                }else{
                    return back();
                }
            }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       if(Doctor::destroy($id)){
            return redirect()->back()->withErrors(['Deletion Successful']);
        }else{
            return redirect()->back()->withErrors(['Deletion Failed']);;
        }
    }

    public function DocQualUpdate(Request $request){
        // return $request;
        $doctor = Doctor::find($request['user_id']);
        if($doctor->qualifications()->sync($request['qualifications'])){
            return redirect()->back()->with('success','Qualification Updation Successful'); 
        }else{
            return redirect()->back()->withErrors(['Updation Successful']);
        }
    } 

}
