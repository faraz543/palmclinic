<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prescription;
use App\Checkup;
use Helper;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class PrescriptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        if($checkup = Checkup::find($id)){
            return $prescription = Prescription::where('checkup_id',$checkup->id)->get();
            // return view('prescription.index');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id){
        if($checkup = Checkup::find($id)){
            $fields = [
                [
                    'label'=>'Date',
                    'name'=>'date',
                    'type'=>'text',
                    'attributes'=>[
                        'class'=> 'form-control datepicker',
                        'id'=>'date',
                        'data-provide'=>'datepicker'
                    ]
                ], 
                [
                    'label'=>'Prescription',
                    'name'=>'prescription',
                    'type'=>'textarea',
                    'attributes'=>[
                        'class' => 'form-control',
                        'id' => 'prescription' 
                    ]
                ],
                [
                    'label'=>'Images',
                    'name'=>'imagesFile[]',
                    'type'=>'file',
                    'attributes'=>[
                        'class' => 'form-control',
                        'id' => 'image',
                        'multiple', 
                    ]
                ],
            ];
            $form_info = ['action'=>['PrescriptionController@store',$id],'method'=>'POST','form_title'=>'Add Prescription','files'=>'files'];
            return view('prescription.add',compact('fields','form_info'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request; 
        $rules = [
            'checkup_id'=>'required|unique:prescriptions',
            'imagesFile'=>'required',
        ];
        $validator = Helper::FormValidation($request,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->messages());
        }else{
            // return $request;
             // return $request;
            // Getting Images in json format
            $images = $request->imagesFile;
            foreach ($images as $img) {
                $data[] = $img->getClientOriginalName();
            }
            $request['images'] = json_encode($data);
            // Storing Prescription
            if($newPrescription = Prescription::create($request->all())){
                if(!empty($images)){
                    foreach ($images as $img) {
                        $img_name = $img->getClientOriginalName();
                        if(Storage::disk('prescription')->put($img_name,File::get($img))){}
                        else{break;}
                    }
                }
                return redirect("/checkups")->with('success','Prescription Added To Checkup');
            }else{
                return redirect()->back()->withErrors(['Addition Failed']);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prescription = Prescription::find($id);
        $pre =  json_decode($prescription['images']);
        return $pre;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Prescription::destroy($id)){
            return Prescription::all();
        }else{
            return 'not found' ;
        }
    }
}
