@extends('layouts.app')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="{{asset('css/dropify.min.css')}}">
@endsection
@section('content')
    <h4 class="card-title">Checkups</h4>
    <div class="container">
        <!-- Error & Success Messages -->
        @include('includes.success-error')
        <!---->
        <div class="col-md-12 table_div">
            <table id='checkup_table' class="display nowrap table table-striped dataTable table-bordered" style="width: 100%">
                <thead>
                    <tr class='thtr'>
                        <th>Date</th>
                        <th>Patient</th>
                        <th>Doctor</th>
                        <th>Reason</th>
                        <th>Prescription</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>

        <!-- Delete Modal -->
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Deletion Confirmation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this record?
                        <p class='testp'></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a class="delConfirm" href="#"><button type="button" class="btn btn-danger">Delete</button></a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Prescription Addition Modal -->
        <div id="AddPrescription" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Add Prescription</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    {!! Form::open(['method'=>'POST','files'=>'true','action'=>'PrescriptionController@store','id'=>'prescriptionForm']) !!}
	                    <div class="modal-body">
	                    	<div class='form-group row'>
	                        	{!! Form::hidden('checkup_id','',['id' => 'checkup_id']) !!}
	                        	{!! Form::label('images','Upload Image/s',['class' => ' col-md-4 col-form-label text-md-right'])!!}
	                        	<div>
	                        	{!! Form::file('imagesFile[]',
	                        	['multiple','class' => 'form-control dropify','id' => 'prescriptionImages',"data-multiple-caption"=>"{count} files selected"]) !!}
	                        	</div>
	                        </div>
	                    </div>
	                    <div class="modal-footer">
	                        <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
	                        <button type="submit" class="btn btn-primary waves-effect waves-light">Add Prescription</button>
	                    </div>
	                {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    @include('includes.yajraTablesJs')
    <script src="{{asset('js/dropify.min.js')}}"></script>
    <script src="{{asset('js/popper.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#checkup_table').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "numbers",
                ajax : 'http://dev.clinic/checkups/getTable',
                columns: [
                    { data: 'date',name:'checkups.date'},
                    { data: 'patientNameNum',name:'patientNameNum',seacrhable:false,sortable:false},
                    { data: 'doctor.name',name:'doctor.name'},
                    { data: 'reason',name:'checkups.reason'},
                    { data: 'prescription',seacrhable:false,sortable:false},
                    { data: 'update',seacrhable:false,sortable:false},
                    { data: 'delete',seacrhable:false,sortable:false},
                    { data: 'patient.name', name: 'patient.name', searchable: true, sortable : true, visible:false},
                    { data: 'patient.phone_number', name: 'patient.phone_number', searchable: true, sortable : true, visible:false},
                ],
            });

            $(document).on('click', '.delModalTrigger',function(){
                $('.delConfirm').attr('href','#');
                var href =  $(this).attr('href');
                $('.delConfirm').attr('href',"{{url('/checkup/')}}"+"/"+href+"/delete");
            });

            $(document).on('click', '.addModalTrigger',function(){
            	$('#prescriptionForm').trigger('reset');
            	$('#checkup_id').val(null);
            	var checkup_id = $(this).attr('href');
            	$('#checkup_id').val(checkup_id);
            });
            $('.dropify').dropify();
        });
    </script>
@endsection