@extends('layouts.app')
@section('extra-css')
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap-clockpicker.min.css')}}">
	<link id="bsdp-css" href="{{asset('css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
	<link id="bsdp-css" href="{{asset('css/jquery-clockpicker.min.css')}}" rel="stylesheet">
@endsection
@section('content')
	<!-- Error & Success Messages -->
    @include('includes.success-error')
    <!---->
	{!! Form::open(['action'=>'CheckupController@store','method'=>'POST','class'=>'jumbotron']) !!}
		<h3 id='form-title' class='form-group'>Add Checkup</h3>
		<div class='form-group row'>";
		    {!! Form::label('search','Patient',['class' => 'col-md-4 col-form-label text-md-right']) !!}
		    <div class='col-md-6'>
			    <select name='patient_id' id="searchField" class="col-md-12"></select>
		    </div>
		</div>
	    @foreach($fields as $field)
		    {!! Form::custom_field($field)!!}
	    @endforeach
	    <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
           			Submit
                </button>
            </div>
        </div>
	{!!Form::close() !!}
@endsection
@section('extra-js')
	<script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
	<script src="{{asset('js/bootstrap-clockpicker.min.js')}}"></script>
	<script src="{{asset('js/jquery-clockpicker.min.js')}}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
	<script type="text/javascript">
		$("#searchField").select2({
			placeholder:"Search Patient By Name / Phone Number / Id",
			minimumInputLength: 1,
			ajax: {
			    url: "{{url('/patientSearch')}}",
			    type:"POST",
			    dataType: 'json',
			    delay: 250, 
			    data: function (params) {
			    	return{
				      search: params.term,
			    	}
			    },
			    processResults:function(data){
			    	return{
			    		results: $.map(data.items,function(val,i){
			    			return{id:val,text:i};
			    		})
			    	}
			    }
			}
		});	
		$('.clockpicker').clockpicker({
	        donetext: 'Done',
	    	}).find('input').change(function() {
	        console.log(this.value);
	    });
	    $('.datepicker').datepicker({
	        maxViewMode: 3,
	    	todayHighlight: true,
	    	autoclose: true,
	    	toggleActive: true,
	    	orientation: "bottom right"
		});
	</script>
@endsection