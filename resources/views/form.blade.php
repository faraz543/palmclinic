@extends('layouts.app')

@section('sidebar')
	@include('includes.sidebar')
@endsection

<?php
	$form_info = ['method'=>'POST','action'=>'TestController@store','form_title'=>'Test Title'];
	$fields = [
		[
			'name'=>'name',
			'type' => 'text',
			// 'value' => null,
			'attributes' =>[
					'class' => 'form-control',
					'id' => 'name'			
				]
		],
		[
			'name'=>'email',
			'type' => 'email',
			// 'value' => null,
			'attributes' =>[
					'class' => 'form-control',
					'id' => 'email'			
				]
		],
		[
			'name'=>'password',
			'type' => 'password',
			'attributes' =>[
					'class' => 'form-control',
					'id' => 'email'			
				]
		],
		[
			'label'=>'Label Name',
			'name'=>'role',
			'type' => 'select',
			'options'=>[
				'first' => '1st',
				'second' => '2nd'
			],
			'attributes' =>[
					'class' => 'form-control',
					'id' => 'email'			
				]
		]
	];

?>

@section('content')

<?php 
	Helper::form_view($fields,$form_info);
?>
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@endsection