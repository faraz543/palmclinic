@extends('layouts.app')
@section('extra-css')
<link id="bsdp-css" href="{{asset('css/bootstrap-datepicker3.min.css')}}" rel="stylesheet">
@endsection
@section('content')
	<?php
		(!empty($fields) && !empty($form_info) ?  Helper::form_view($fields,$form_info) :  ''); 
	?>
	@if ($errors->any())
	    <div class="alert alert-danger">
	        <ul>
	            @foreach ($errors->all() as $error)
	                <li>{{ $error }}</li>
	            @endforeach
	        </ul>
	    </div>
	@endif
    <!-- <input type="text" class="form-control datepicker" data-provide='datepicker'> -->
@endsection
@section('extra-js')
  <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/nicEdit-latest.min.js')}}"></script> 
  <script type="text/javascript">
	//<![CDATA[
        bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
  	//]]>
  	 $('.datepicker').datepicker({
        maxViewMode: 3,
    	todayHighlight: true,
    	autoclose: true,
    	toggleActive: true
	});
  </script>

@endsection
