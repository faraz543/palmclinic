@extends('layouts.app')
@section('extra-css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" />
@endsection
@section('content')
    <div class="card-title">Home</div>

    <div class="card-body">
		{!! Form::open(['action'=>"CheckupController@search",'method'=>'POST','id'=>'searchPrescription']) !!}
	    	<select name='search' id="searchField" class="col-md-7 col-xs-12"></select>
	    	{!! Form::select('doctor_id',$doctors,null,['class'=>'form-group col-md-3 col-xs-12','id'=>'doctorId','placeholder'=>'Select Doctor']) !!}
	    	<button type='submit' class='btn btn-primary '>submit</button>
	    {!! Form::close() !!}
        <div class="records"></div>
        <div id="ImageView" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Prescription</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <img src="" alt="no img" id="prescriptionImage">
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>
<script type="text/javascript">
	$("#searchField").select2({
		placeholder:"Search Patient By Name / Phone Number / Id",
		minimumInputLength: 1,
		ajax: {
		    url: "{{url('/patientSearch')}}",
		    type:"POST",
		    dataType: 'json',
		    delay: 250, 
		    data: function (params) {
		    	return{
			      search: params.term,
		    	}
		    },
		    processResults:function(data){
		    	return{
		    		results: $.map(data.items,function(val,i){
		    			return{id:val,text:i};
		    		})
		    	}
		    }
		}	    
	});
	$("#doctorId").select2({});
	$(document).on('submit', '#searchPrescription',function(){
		$('.records').empty();
		event.preventDefault();
		var formData = {
            'search'	: $('#searchField').val(),
            'doctor_id' : $('#doctorId').val(),
        };
        var checkupCard = function(checkup){
        	var div = "<div class='col-md-6'>"+
	            "<div class='card card-outline-inverse'>"+
	                "<div class='card-header'>"+
	                    "<h4 class='m-b-0 text-white'>Date: "+checkup.date+"</h4></div>"+
	                "<div class='card-body'>"+
	                    "<h3 class='card-title'>Reason: "+checkup.reason+"</h3>"+
	                    "<div class='buttons-div'>"
	                    if(checkup.prescription == null){
	                    	div += "<button class='btn btn-warning'>No Prescription</button>&nbsp;&nbsp;"+
	                    	"<a class='btn btn-info' href=checkup/"+checkup.id+"/addprescription>+</a>";
	                    }else{
	                    	var images = JSON.parse(checkup.prescription.images); 
	                    	// alert(images.length);
	                    	// $.each(images,function(i,item){})
	                    	div += "<a id='imgTrigger' data-toggle='modal' data-target='#ImageView' image="+images+" href=checkup/"+checkup.id+"/prescription class='btn btn-primary'>View Prescription</a>";
	                    }
	                    div +="</div><table class='table'>"+
	                    	"<tr class='table-success'>"+
	                    		"<td>Start Time</td>"+
	                    		"<td>"+checkup.start_time+"</td>"+
	                    	"</tr>"+
	                    	"<tr class='table-danger'>"+
	                    		"<td>End Time</td>"+
	                    		"<td>"+checkup.end_time+"</td>"+
	                    	"</tr>"+
	                    "</table>"+
	                "</div>"+
	            "</div>"+
            "</div>";
            return div;
        }
        	
		$.ajax({
			url:"{{url('/checkup/search')}}",
			type:"POST",
			data: formData,
			dataType:'json',
		})
		.done(function(data){
			$.each(data,function(i,item){
				$('.records').append(checkupCard(data[i]));
			})
		});
		var env = "{{env('PRESCRIPTION')}}";
		$(document).on('click', '#imgTrigger',function(){
			// alert('here');
			var src = $(this).attr('image');
			$('#prescriptionImage').attr('src','');
			event.preventDefault();
			$('#prescriptionImage').attr('src',env+src);
		})

	});

</script>

@endsection