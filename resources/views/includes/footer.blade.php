    </div>
    <!-- End Wrapper -->

    <!-- All Jquery -->
    <script src=" {{ asset('js/jquery.min.js')}}"></script>
    <!-- Bootstrap tether Core JavaScript -->
    <script src=" {{ asset('js/popper.min.js')}}"></script>
    <script src=" {{ asset('js/bootstrap.min.js')}}"></script>
    <!-- slimscrollbar scrollbar JavaScript -->
    <script src=" {{ asset('js/jquery.slimscroll.js') }}"></script>
    <!--Wave Effects -->
    <script src=" {{ asset('js/waves.js') }}"></script>
    <!--Menu sidebar -->
    <script src=" {{ asset('js/sidebarmenu.js') }}"></script>
    <!--stickey kit -->
    <script src=" {{ asset('js/sticky-kit.min.js') }}"></script>
    <script src=" {{ asset('js/jquery.sparkline.min.js') }}"></script>
    <!--Custom JavaScript -->
    <script src=" {{ asset('js/custom.min.js') }}"></script>
    <!-- Style switcher -->
    <script src=" {{ asset('js/jQuery.style.switcher.js') }}"></script>
    @yield('extra-js')
</body>

</html>