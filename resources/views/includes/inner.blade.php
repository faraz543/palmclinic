<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb and right sidebar toggle -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-themecolor">Clinic</h3>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->

    </div>
    <!-- End Container fluid  -->

    <!-- footer -->
    <footer class="footer">
        © Faujeeks
    </footer>
    <!-- End footer -->
</div>
<!-- End Page wrapper  -->