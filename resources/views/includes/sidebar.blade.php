<!-- Left Sidebar - style you can find in sidebar.scss  -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <div class="user-profile">
            <div class="profile-img"> 
                     <p style='text-align: center;'><i class="mdi mdi-account-circle mdi-48px"></i></p>
            </div>
            <div class="profile-text"> 
                <h5 class="lead text-capitalize">{{Auth::user()->name}}</h5>
                <!-- <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="mdi mdi-settings"></i></a> -->
                <a href="{{ route('logout') }}" class="" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
                <!-- <div class="dropdown-menu animated flipInY">
                    <a href="#" class="dropdown-item"><i class="ti-user"></i> My Profile</a>
                    <a href="#" class="dropdown-item"><i class="ti-wallet"></i> My Balance</a>
                    <a href="#" class="dropdown-item"><i class="ti-email"></i> Inbox</a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="ti-settings"></i> Account Setting</a>
                    <div class="dropdown-divider"></div>
                    <a href="login.html" class="dropdown-item"><i class="fa fa-power-off"></i> Logout</a>
                </div> -->
            </div>
        </div>
        <!-- End User profile -->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" >
                <li class="nav-devider"></li>
                <li> 
                    <a class="waves-effect home" href="{{url('/')}}" >
                        <span class="hide-menu"><i class="mdi mdi-home"></i></span>
                    </a>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <span class="hide-menu">Doctors</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/doctors')}}">View All</a></li>
                        <li><a href="{{url('/adddoctor')}}">Add Doctor +</a></li>
                    </ul>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <span class="hide-menu">Patients</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/patients')}}">View All</a></li>
                        <li><a href="{{url('/addpatient')}}">Add Patient +</a></li>
                    </ul>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <span class="hide-menu">Qualifications</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/qualifications')}}">View All</a></li>
                        <li><a href="{{url('/addqualification')}}">Add Qualification +</a></li>
                    </ul>
                </li>
                <li> 
                    <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false">
                        <span class="hide-menu">Checkups</span>
                    </a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('/checkups')}}">View All</a></li>
                        <li><a href="{{url('/addcheckup')}}">Add Checkup +</a></li>
                    </ul>
                </li>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
