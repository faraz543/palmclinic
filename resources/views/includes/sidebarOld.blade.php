<div class="col-md-3">
    <div class="card">
        <div class="card-header">Pages</div>
            
        <div class="card-body">
            <?php
            	$pages = [
            		'+Add Users' => '/adduser',
            		'Test Blade' => '/test',
            		'Admin' => '/admin',
            		'Doctor' => '/doctor',
                    'Patients' => '/patients',
                    '+Add Patients'=>'/addpatient'
            	]
            ?>
			<ul>
                @foreach ($pages as $page_name => $url)
                	<li><a href="{{$url}}">{{$page_name}}</a></li>
            	@endforeach
        	</ul>
        </div>
    </div>
</div>