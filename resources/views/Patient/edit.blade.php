@extends('layouts.app')
@section('content')
	<div class="container">
		<!-- Error & Success Messages -->
        @include('includes.success-error')
        <!---->
		<?php
			// Patient Info Form
			(!empty($fields) && !empty($form_info) ?  Helper::form_view($fields,$form_info) :  ''); 
			
		?>
	</div>
@endsection
