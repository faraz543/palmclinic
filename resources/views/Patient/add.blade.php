@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<?php
	(!empty($fields) && !empty($form_info) ?  Helper::form_view($fields,$form_info) :  ''); 
?>
@endsection