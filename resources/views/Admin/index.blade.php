<!-- ADMIN -->
@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">Pages</div>
                    
                <div class="card-body">
                    
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    This is Admin Dashboard. You must be privileged to be here !
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection