@extends('layouts.app')

@section('sidebar')
        @include('includes.sidebar')
@endsection
@section('content')
    <div class="card-header">Add User</div>

    <div class="card-body">
        {!! Form::open(['method'=>'post','action'=>'AdminController@UserStore']) !!}
            @csrf

            <div class="form-group row">
                {!! Form::label('name', 'Name', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                <div class="col-md-6">
                   {!!  Form::text('name','',['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('email', 'E-mail', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                <div class="col-md-6">
                   {!!  Form::email('email','',['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('password', 'Password', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                <div class="col-md-6">
                   {!!  Form::password('password',['class' => 'form-control','required']) !!}
                </div>
            </div>
            <div class="form-group row">
                {!! Form::label('role', 'Role', ['class' => 'col-md-4 col-form-label text-md-right']) !!}

                <div class="col-md-6">
                   {!!  Form::select('role',['1'=>'Admin','2'=>'Doctor','3'=>'Patient'],'3',['class' => 'form-control','required']) !!}
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-primary">
                        {{ __('Register') }}
                    </button>
                </div>
            </div>
        {!! form::close() !!}
    </div>
@endsection
