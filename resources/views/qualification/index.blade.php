@extends('layouts.app')

@section('content')
    <h4 class="card-title">Qualifications</h4>
    <div class="container">
        <!-- Error & Success Messages -->
        @include('includes.success-error')
        <!---->
        <div class="col-md-12 table_div">
            <table id='qual_table' class="display nowrap table table-striped dataTable table-bordered" style="width: 100%">
                <thead>
                    <tr class='thtr'>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Date</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
        <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="exampleModalLabel1">Deletion Confirmation</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        Are you sure you want to delete this record?
                        <p class='testp'></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <a class="delConfirm" href="#"><button type="button" class="btn btn-danger">Delete</button></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('extra-js')
    @include('includes.yajraTablesJs')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#qual_table').DataTable({
                processing: true,
                serverSide: true,
                pagingType: "numbers",
                ajax : 'http://dev.clinic/qualifications/getTable',
                columns: [
                    { data: 'name',name:'name'},
                    { data: 'description',name:'description'},
                    { data: 'created_at',name:'created_at'},
                    { data: 'delete'},
                ],
            });

            $(document).on('click', '.delModalTrigger',function(){
                $('.delConfirm').attr('href','#');
                var href =  $(this).attr('href');
                $('.delConfirm').attr('href',"{{url('/qualification/')}}"+"/"+href+"/delete");
            });

        });
    </script>


@endsection
