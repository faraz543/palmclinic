@extends('layouts.app')
@include('includes.select2')
@section('content')
	<div class="container">
		<!-- Error & Success Messages -->
        @include('includes.success-error')
        <!---->
		<?php
			// Doctor Info Form
			(!empty($fields) && !empty($form_info) ?  Helper::form_view($fields,$form_info) :  ''); 
			// Qualification Update Form
			(!empty($qualification_fields) && !empty($qualification_form) ?  Helper::form_view($qualification_fields,$qualification_form) :  ''); 
		?>
	</div>
@endsection
