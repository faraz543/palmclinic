@extends('layouts.app')
@include('includes.select2')
@section('content')
	<!-- Error & Success Messages -->
    @include('includes.success-error')
    <!---->
	<?php
		(!empty($fields) && !empty($form_info) ?  Helper::form_view($fields,$form_info) :  ''); 
	?>
@endsection

